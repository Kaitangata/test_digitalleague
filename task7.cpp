#include <iostream>

int reverse(int num, int digit) {
	if (!digit) {
		return -1;
	}
	int res = reverse(num % digit, digit / 10);
	if (res == -1) {
		return num / digit;
	}
	else {
		return res * 10 + num / digit;
	}
		
}

int main() {
	int res = 0;
	int temp;
	for (int i = 10000; i <= 99999;i++) {
		temp = reverse(i, 10000);
		if (temp == i) {
			std::cout << i << '\n';
			res++;
		}
	}
	std::cout << res;
	return 0;
}