#include <iostream>
bool even(int num) {
	return (num % 2);
		
}
bool recursion(int num, int digit) {
	if (!digit) {
		return 1;
	}
	if (even(num / digit)) {
		return recursion(num % digit, digit / 10);
		
	}
	return 0;
}

int main() {
	int res = 0;
	for (int i = 1000; i <= 9999; i++) {
		
		if (recursion(i, 1000)) {
			res++;
			std::cout << i << '\n';
		}
	}
	
	std::cout << res;
	return 0;
}